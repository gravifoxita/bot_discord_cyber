# Create by gravifoxita #

import requests
import json
import asyncio
import xml.etree.ElementTree as ET
import datetime
import discord
import xmltodict

intents = discord.Intents.all()
client = discord.Client(intents=intents)

async def get_cybernews_updates(url): 
    response = requests.get(url)
    data = response.text
    try:
        root = ET.fromstring(data)
        items = root.findall('./channel/item')
        updates = []
        for item in items:
            title = item.find('title').text
            link = item.find('link').text
            updates.append({'title': title, 'link': link})
        return updates
    except ET.ParseError as e:
        return [{'title': 'Erreur', 'link': str(e)}]

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    elif message.content.startswith('!cyberupdates'):
        try:
            updates = await get_cybernews_updates('YOUR_LINK')
            for update in updates:
                update_title = update['title']
                update_link = update['link']
                await message.channel.send('News: {0}\nLink: {1}'.format(update_title, update_link))
        except Exception as e:
            await message.channel.send('Error: {0}'.format(str(e)))
    

client.run('YOUR_TOKEN_API_KEY')